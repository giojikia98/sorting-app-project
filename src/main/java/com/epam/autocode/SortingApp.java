package com.epam.autocode;


import java.util.Arrays;
import java.util.Scanner;

public class SortingApp {
    public static void main( String[] args ){
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String[] items = input.split(" ");

        int[] arguments = new int[items.length];
        for (int i = 0; i < items.length; i++) arguments[i] = Integer.parseInt(items[i]);

        Arrays.sort(arguments);
        System.out.println(Arrays.toString(arguments));
    }
    public static String sort(int[] array) {
        if (array.length == 0) throw new IllegalArgumentException();
        if (array.length == 1) throw new IllegalArgumentException();
        if (array.length > 10) throw new IllegalArgumentException();
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int temp;
                if (array[j] < array[i]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return Arrays.toString(array);
    }
}
