package com.epam.autocode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {
    protected SortingApp sortingApp = new SortingApp();

    private int[] array;
    private String expected;

    public SortingAppTest (int[] array, String expected){
        this.array = array;
        this.expected = expected;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testzerocase(){
        array = new int[]{};
        SortingApp.sort(array);
        assertEquals(0, array.length);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOnecase(){
        array = new int[]{3};
        SortingApp.sort(array);
        assertEquals(1, array.length);
        assertEquals(3, array[0]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testmorethantencase(){
        array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        SortingApp.sort(array);
        assertEquals(11, array.length);
    }


    @Parameterized.Parameters
    public static Collection data(){
        int[] array1 = {1, -2, 5, 3, 9, 4},
                array2 = {10, 4, -5, 0, 20},
                array3 = {55, 45, -1, 1, 0, 10},
                array4 = {5, 25, 9, 13, 10},
                array5 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        return Arrays.asList(new Object[][]{
                {array1, "[-2, 1, 3, 4, 5, 9]"},
                {array2, "[-5, 0, 4, 10, 20]"},
                {array3, "[-1, 0, 1, 10, 45, 55]"},
                {array4, "[5, 9, 10, 13, 25]"},
                {array5, "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]"}
        });
    }


    @Test
    public void testlessthantencase(){

        assertEquals(expected, SortingApp.sort(array));
    }

}